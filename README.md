<div align="center">
<img src="https://gitlab.com/AOSkjP/AdditionalDocs/raw/pie/AOSkjP-Logo-Git.png">
</div>

Introduction
---------------
A personal project designed with the idea of ​​keeping everything clean and simple, most close to AOSP concept. Without neglecting the necessary updates to maintain a stable, secure and fast system.

Sync
---------------

```bash
repo init -u https://gitlab.com/AOSkjP/platform_manifest.git -b pie
```
Then to sync up:
```bash
repo sync -f --force-sync --no-tags --no-clone-bundle
```

Building
---------------

```bash
. build/envsetup.sh
lunch aoskjp_devicename-userdebug
mka bacon
```

Credits
---------------

- LineageOS
- SuperiorOS
- AEX
- Syberia
- LiquidRemix
- ArrowOS
- AOSiP
- PixelExperience
- And to anyone who has crossed on my way and offered their help, thanks!
